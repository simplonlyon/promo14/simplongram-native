import { createSlice } from "@reduxjs/toolkit";
import { authApi } from "./auth-api";
import { User } from "./entities";
import * as SecureStore from 'expo-secure-store';

export interface AuthState {
    user: User | null;
    token: string | null;
}

const initialState: AuthState = {
    user: null,
    token: null
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setCredentials(state, { payload }) {
            state.user = payload.user;
            state.token = payload.token
        }
    },
    extraReducers: (builder) => {
        builder.addMatcher(
            authApi.endpoints.login.matchFulfilled,
            (state, { payload }) => {
                state.token = payload.token
                state.user = payload.user
                if (payload.token) {
                    SecureStore.setItemAsync('token', payload.token);
                }
            }
        )
    }
});

export default authSlice.reducer;
export const { setCredentials } = authSlice.actions