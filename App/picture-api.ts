import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'
import { Picture } from './entities'
import Constants from 'expo-constants';
import { RootState } from './store';

export interface PictureForm {
    description:string;
    file:string;
}

export const pictureApi = createApi({
    reducerPath:'pictureApi',
    baseQuery: fetchBaseQuery({
        baseUrl: Constants.manifest?.extra?.SERVER_URL+'/api/picture',
        prepareHeaders: (headers, { getState }) => {
            const token = (getState() as RootState).auth.token
            if (token) {
              headers.set('authorization', `Bearer ${token}`)
            }
            return headers
          }
    }),
    tagTypes: ['PictureList'],
    endpoints: (builder) => ({
        getAllPictures: builder.query<Picture[], number|void>({
            query: (page=1) => '?page='+page,
            providesTags: ['PictureList']
        }),
        postPicture: builder.mutation<Picture, PictureForm>({
            query:(body) => ({
                url: '/',
                method:'POST',
                body
            }),
            invalidatesTags: ['PictureList']
        })
    })
});

export const {useGetAllPicturesQuery, usePostPictureMutation} = pictureApi;