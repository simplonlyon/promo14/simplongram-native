import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'
import { User } from './entities'
import Constants from 'expo-constants';
import {AuthState} from './auth-slice'


export interface Credentials {
    username:string;
    password:string;
}

export const authApi = createApi({
    reducerPath:'authApi',
    baseQuery: fetchBaseQuery({
        baseUrl: Constants.manifest?.extra?.SERVER_URL+'/api/user'
    }),
    endpoints: (builder) => ({
        login: builder.mutation<AuthState, Credentials>({
            query: (body) => ({
                url: '/login',
                method: 'POST',
                body
            }),
        })
    })
});

export const {useLoginMutation} = authApi;