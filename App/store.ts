import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { authApi } from './auth-api';

import { pictureApi } from './picture-api';
import authSlice from './auth-slice';

export const store = configureStore({
  reducer: {
    [pictureApi.reducerPath]:pictureApi.reducer,
    [authApi.reducerPath]:authApi.reducer,
    auth:authSlice
  },
  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware().concat(pictureApi.middleware, authApi.middleware),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
