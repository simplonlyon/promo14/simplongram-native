export interface User {
    id?: string;
    username?:string;
    password?:string;
    avatar?:string;
    favorites?: Picture[];
    pictures?:Picture[];
}

export interface Picture {
    id?:string;
    description?:string;
    filename?:string;
    imageUrl?:string;
    thumbnailUrl?:string;
    postDate?:string|Date;
    likes?:User[];
    author?:User;
}