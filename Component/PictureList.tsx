import { FlatList, RefreshControl, ScrollView, Text, View } from "react-native";
import { useGetAllPicturesQuery } from "../App/picture-api";
import React, { useState } from 'react';
import { PictureCard } from "./PictureCard";
import { Button } from "react-native-elements";




export function PictureList() {
    const [page, setPage] = useState(1);
    const { data, isLoading, isError, refetch, } = useGetAllPicturesQuery(page);

    if (isLoading) {
        return (
            <View>
                <Text>Loading...</Text>
            </View>
        )
    }
    if (isError) {
        return (
            <View>
                <Text>Oups...</Text>
            </View>
        )

    }

    return (
        <FlatList data={data} 
        refreshControl={<RefreshControl refreshing={isLoading} onRefresh={refetch} />} 
        renderItem={({item}) => <PictureCard key={item.id} picture={item} />}
        ListFooterComponent={<Button title="More" onPress={() => setPage(p => p+1)}></Button>}>
        </FlatList>
    );
}