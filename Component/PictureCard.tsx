import { StyleSheet, Text } from "react-native";
import { Card } from "react-native-elements";
import { Picture } from "../App/entities";
import Constants from 'expo-constants';
import React from 'react';

interface Props {
    picture: Picture;
}

export function PictureCard({ picture }: Props) {

    return (
        <Card>
            <Card.Image source={{ uri: picture.imageUrl }}></Card.Image>
            <Text>{picture.description}</Text>
            <Text style={styles.author}>{picture.author?.username}</Text>
        </Card>
    );
}

const styles = StyleSheet.create({
    author: {
        textAlign: 'right',
        fontSize: 10
    }
})