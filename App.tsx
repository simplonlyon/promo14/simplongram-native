import { createNativeStackNavigator, NativeStackNavigationProp } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { HomeScreen } from './Screen/HomeScreen';
import { Provider } from 'react-redux';
import { store } from './App/store';
import { LoginScreen } from './Screen/LoginScreen';
import { AddPictureScreen } from './Screen/AddPictureScreen';


const Stack = createNativeStackNavigator();

export interface NavProps {
  navigation: NativeStackNavigationProp<any>
}

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen name="Home" component={HomeScreen} options={{headerBackVisible:false}} />
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="AddPicture" component={AddPictureScreen} />

        </Stack.Navigator>

      </NavigationContainer>
    </Provider>
  );
}
