import { View, Text } from "react-native";
import { Button, Input } from "react-native-elements";
import { NavProps } from "../App";
import React, { useEffect } from 'react';
import { Controller, useForm } from "react-hook-form";
import { Credentials, useLoginMutation } from "../App/auth-api";
import { useSelector } from "react-redux";
import { RootState } from "../App/store";
import * as SecureStore from 'expo-secure-store';


export function LoginScreen({ navigation }: NavProps) {
    const user = useSelector<RootState>(state => state.auth.user);
    const [login, { isLoading, error }] = useLoginMutation();
    const { control, handleSubmit, formState } = useForm();
    const onSubmit = async (data: Credentials) => {
        try {
            await login(data).unwrap();
            SecureStore.setItemAsync('credentials', JSON.stringify(data));
        } catch (error) { }

    }
    useEffect(() => {
        if (user) {
            navigation.navigate('Home');
        }
    }, [user]);

    useEffect(() => {
        (async () => {
            const credentials = await SecureStore.getItemAsync('credentials');
            if (credentials) {
                login(JSON.parse(credentials));
            }
        })();
    }, []);

    const loginError = () => {
        if(error && 'status' in error) {
            if(error.status === 401) {
                return <Text>Credentials Error</Text>
            }else {
                return <Text>Login server error</Text>
            }
        }
    }

    return (
        <View>
            {loginError()}
            <Controller name="username" control={control}
                rules={{ required: true }}
                render={({ field }) => (
                    <Input onChangeText={field.onChange}
                        autoCapitalize="none"
                        placeholder="Username"
                        value={field.value} 
                        errorMessage={formState.errors.username && "Username is required"} />
                )} />

            <Controller name="password" control={control}
                rules={{ required: true }}
                render={({ field }) => (
                    <Input secureTextEntry
                        autoCapitalize="none"
                        placeholder="Password"
                        onChangeText={field.onChange}
                        value={field.value}
                        errorMessage={formState.errors.password && "Password is required"} />
                )} />


            <Button onPress={handleSubmit(onSubmit)} loading={isLoading} title="Login" />
        </View>
    );
}