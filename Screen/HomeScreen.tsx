import {  View } from "react-native";
import React from 'react';
import { PictureList } from "../Component/PictureList";
import {FAB} from 'react-native-elements'
import { NavProps } from "../App";

export function HomeScreen({navigation}:NavProps) {
   

    return (
        <View>
            <PictureList />
            <FAB color="red" placement="right" size="large" title="+" onPress={() => navigation.navigate('AddPicture')} />
        </View>
    )
}