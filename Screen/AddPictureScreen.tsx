import React, { useEffect, useState } from 'react';
import { Image, Text, View } from 'react-native';
import { Button, FAB, Input } from 'react-native-elements';
import * as ImagePicker from 'expo-image-picker';
import { usePostPictureMutation } from '../App/picture-api';
import { NavProps } from '../App';



export function AddPictureScreen({ navigation }: NavProps) {
    const [postPicture, { isLoading, data, error, isSuccess }] = usePostPictureMutation();
    const [description, setDescription] = useState('');
    const [file, setFile] = useState('');

    useEffect(() => {
        ImagePicker.requestCameraPermissionsAsync();
    }, []);
    const handlePicture = async () => {
        const result = await ImagePicker.launchCameraAsync({ base64: true });
        if (!result.cancelled && result.base64) {
            setFile(result.base64);
        }
    }

    const handleSubmit = async () => {
        await postPicture({
            description,
            file
        });
    }
    useEffect(() => {
        if (isSuccess) {
            navigation.goBack();
        }
    }, [isSuccess])

    return (
        <View style={{ height: '100%' }}>
            <Text>Add Picture</Text>
            <Input label="Description"
                onChangeText={val => setDescription(val)}
                value={description} />

            {file.length > 0 && <Image style={{ width: '100%', height: 400, resizeMode: 'cover' }} source={{ uri: 'data:image/png;base64,' + file }} />}
            <Button title="Take Picture" onPress={handlePicture} />
            <FAB color="red" placement="right" size="large" title="+" onPress={handleSubmit} />

        </View>
    )
}